import { defineStore } from 'pinia'
// Import axios to make HTTP requests
import axios from "axios"

export const useCardStore = defineStore("cards", {
    state: () => ({
        releases: [],
        heroes: [],
        encounterSets: [],
        scenarios: []
    }),
    getters: {
        curatedReleases(state) {
            let curatedReleases = []
            for(let i in state.releases) {
                curatedReleases.push(state.releases[i])
                curatedReleases[i].imgUrl = new URL('../assets/img/releases/' + state.releases[i].cover_basename + '.png', import.meta.url).href
            }
            return curatedReleases
        },
        curatedHeroes(state){
            let curatedHeroes = []
            for(let i in state.heroes) {
                curatedHeroes.push(state.heroes[i])
                curatedHeroes[i].type = "Hero"
                curatedHeroes[i].name = state.heroes[i].hero_name + " / " + state.heroes[i].alterego_name
                curatedHeroes[i].imgUrl = new URL('../assets/img/heroes/' + state.heroes[i].cover_basename + '.png', import.meta.url).href
                curatedHeroes[i].iconUrl = new URL('../assets/img/heroes/' + state.heroes[i].cover_basename + '_ico.png', import.meta.url).href
            }
            return curatedHeroes
        },
        curatedScenarios(state){
            let curatedScenarios = []
            for(let i in state.scenarios) {
                curatedScenarios.push(state.scenarios[i])
                curatedScenarios[i].type = "Scenario"
                curatedScenarios[i].imgUrl = new URL('../assets/img/scenarios/' + state.scenarios[i].cover_basename + '.png', import.meta.url).href
                curatedScenarios[i].iconUrl = new URL('../assets/img/scenarios/' + state.scenarios[i].cover_basename + '_ico.png', import.meta.url).href
            }
            return curatedScenarios
        },
        curatedEncounterSets(state){
            let curatedEncounterSets = []
            for(let i in state.encounterSets) {
                curatedEncounterSets.push(state.encounterSets[i])
                curatedEncounterSets[i].type = "Encounter Set"
                curatedEncounterSets[i].imgUrl = new URL('../assets/img/encountersets/' + state.encounterSets[i].cover_basename + '.png', import.meta.url).href
                curatedEncounterSets[i].iconUrl = new URL('../assets/img/encountersets/' + state.encounterSets[i].cover_basename + '_ico.png', import.meta.url).href
            }
            return curatedEncounterSets
        },
    },
    actions: {
        async fetchReleases() {
            try {
                const data = await axios.get(import.meta.env.VITE_BACKEND_URL + '/api/cards/releases/')
                this.releases = data.data
            }
            catch (error) {
                alert(error)
                console.log(error)
            }
        },
        async fetchHeroes() {
            try {
                const data = await axios.get(import.meta.env.VITE_BACKEND_URL + '/api/cards/heroes/')
                this.heroes = data.data
            }
            catch (error) {
                alert(error)
                console.log(error)
            }
        },
        async fetchEncounterSets() {
            try {
                const data = await axios.get(import.meta.env.VITE_BACKEND_URL + '/api/cards/encountersets/')
                this.encounterSets = data.data
            }
            catch (error) {
                alert(error)
                console.log(error)
            }
        },
        async fetchScenarios() {
            try {
                const data = await axios.get(import.meta.env.VITE_BACKEND_URL + '/api/cards/scenarios/')
                this.scenarios = data.data
            }
            catch (error) {
                alert(error)
                console.log(error)
            }
        },
        async fetchAll() {
            this.fetchReleases();
            this.fetchHeroes();
            this.fetchEncounterSets();
            this.fetchScenarios();
        },
        getHeroesByRelease(release) {
            return this.heroes.filter(a => a.release == release.url)
        },
        getEncounterSetsByRelease(release) {
            return this.encounterSets.filter(a => a.release == release.url)
        },
        getScenariosByRelease(release) {
            return this.scenarios.filter(a => a.release == release.url)
        },
        getReleaseById(release_id) {
            return this.releases.find(a => a.id == release_id)
        },
        getReleaseByCard(card){
            return this.releases.find(a => a.url == card.release)
        },
        getEncounterSetByUrl(encounterSet) {
            return this.encounterSets.find(a => a.url == encounterSet)
        }

    },
})
import { defineStore } from 'pinia'

export const useNavStore = defineStore("nav", {
    state: () => ({
        navigation: [
            { name: 'Home', href: '/', current: true },
            { name: 'Collection', href: '/collection', current: false },
            { name: 'Matches', href: '/matches', current: false },
            { name: 'Cards', href: '/cards', current: false }
        ]
    }),
    getters: {
        currentNav(state) {
            return this.navigation.find(a => a.current === true).name
        }
    },
    actions: {
        menuChange(name) {
            for (let item in this.navigation) {
                this.navigation[item].current = false
            }
            this.navigation.find(a => a.name === name).current = true
        }
    },
    persist: {
        key: 'navigation',
    },
});
import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import CollectionView from '@/views/CollectionView.vue'
import MatchView from '@/views/MatchView.vue'
import CardsView from '@/views/CardsView.vue'

const router = createRouter({
  mode: 'history',
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/collection',
      name: 'collection',
      component: CollectionView
    },
    {
      path: '/matches',
      name: 'matches',
      component: MatchView
    },
    {
      path: '/cards',
      name: 'cards',
      component: CardsView
    }
  ]
})

export default router

import { ref, watch } from "vue";

export default function useStorage(key, val = null) {

    let storedValue = localStorage.getItem(key);

    if (storedValue) {
        val = ref(storedValue);
    } else {
        val = ref(val);
        write()
    }
    

    watch(val, () => {
        write() ;
    });

    function write() {
        if (val.value === null || val.value === ''){
            localStorage.removeItem(key);
        } else {
            localStorage.setItem(key, val.value);
        }
        
    }

    return val

    
}